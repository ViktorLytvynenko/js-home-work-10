let ulTitleElements = document.querySelector(".tabs");
let liTitleElements = ulTitleElements.querySelectorAll("li");

let ulContentElements = document.querySelector(".tabs-content");
let liContentElements = ulContentElements.querySelectorAll("li");

function titleColour() {
    for (let i = 0; i < liTitleElements.length; i++) {
        liTitleElements[i].addEventListener("click", function() {
            for (let j = 0; j < liTitleElements.length; j++) {
                liTitleElements[j].classList.remove("active");
                liContentElements[j].classList.remove("active");
            }
            liTitleElements[i].classList.add("active");
            liContentElements[i].classList.add("active");

        })
    }
}
titleColour(liTitleElements);



